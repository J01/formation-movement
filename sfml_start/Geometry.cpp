#include "Geometry.h"

//Function to find the orientation of three points
const Orientation GetOrientation(const Dim2D& p1, const Dim2D& p2, const Dim2D& p3)
{
    float a = (p2.y - p1.y) / (p2.x - p1.x);
    float b = (p3.y - p2.y) / (p3.x - p2.x);

    return (a == b) ? COLINEAR :
        (a > b) ? CLOCKWISE :
        ANTICLOCKWISE;
}

//Function to find if a given point is on a line segment
const bool LineSegment::onSegment(const Pos& pnt)
{
    //pnt.x = pnt1.x + a * (pnt2.x - pnt1.x)
    float xDiff = pnt2.x - pnt1.x;
    if (xDiff == 0 && pnt.x == pnt1.x)
    {
        //proportion of y change relative to segment
        float a = (pnt.y - pnt1.y) / (pnt2.y - pnt1.y);
        if (0 <= a && a <= 1)
        {
            return true;
        }
    }
    else
    {
        float a = (pnt.x - pnt1.x) / xDiff;
        if (0 <= a && a <= 1)
        {
            float trialY = pnt1.y + a * (pnt2.y - pnt1.y);
            return (trialY == pnt.y);
        }
    }
    return false;
}

//function to find if two given line segments intersect
const bool Intersect(const LineSegment& l1, const LineSegment& l2)
{
    //line 1 start to line 2 start to line 1 end
    Orientation o1 = GetOrientation(l1.pnt1, l2.pnt1, l1.pnt2);
    //line 1 start to line 2 start to line 2 end
    Orientation o2 = GetOrientation(l1.pnt1, l2.pnt1, l2.pnt2);
    //line 1 end to line 2 end to line 1 start
    Orientation o3 = GetOrientation(l1.pnt2, l2.pnt2, l1.pnt1);
    //line 1 end to line 2 end to line 2 start
    Orientation o4 = GetOrientation(l1.pnt2, l2.pnt2, l2.pnt1);

    // o1
    //  1.1       2.2
    //   | \    /
    //   |  \  /
    //   |   \/
    //   |   /\
    //   |  /  \
    //   V /    \
    //  2.1 ---> 1.2
    //
    //o2
    //  1.1       2.2
    //   | \    >
    //   |  \  /
    //   |   \/
    //   |   /\
    //   |  /  \
    //   V /    \
    //  2.1      1.2
    //
    //o3
    //  1.1 <---  2.2
    //     \    /  ^
    //      \  /   |
    //       \/    |
    //       /\    |
    //      /  \   |
    //     /    \  |
    //  2.1       1.2
    //
    //o4
    //  1.1       2.2
    //     \    /  ^
    //      \  /   |
    //       \/    |
    //       /\    |
    //      /  \   |
    //     <    \  |
    //  2.1       1.2

    //General case
    return ((o1 != o2 && o3 != o4)
        /* colinear special cases*/
        || (o1 == COLINEAR && LineSegment{ l1.pnt1, l2.pnt1 }.onSegment(l1.pnt2))
        || (o2 == COLINEAR && LineSegment{ l1.pnt1, l2.pnt1 }.onSegment(l2.pnt2))
        || (o3 == COLINEAR && LineSegment{ l2.pnt2, l2.pnt2 }.onSegment(l1.pnt1))
        || (o4 == COLINEAR && LineSegment{ l1.pnt2, l2.pnt2 }.onSegment(l2.pnt1)));
}

//Function to find the intersection of two given line segments, giving a return value through out
const bool Intersect(const LineSegment& l1, const LineSegment& l2, Pos*& out)
{
    if (out)
    {
        delete out;
        out = nullptr;
    }

    //using cross product
    // l1.pnt1 <- p     pnt1 to pnt 2 <- r      l1 : p + kr
    // l2.pnt1 <- q     pnt1 to pnt 2 <- s      l2 : q + us

    bool toReturn = false;

    Dim2D pToQ = l2.pnt1 - l1.pnt1;
    Vector r = l1.pnt2 - l1.pnt1;
    Vector s = l2.pnt2 - l2.pnt1;

    float rXs = cross(r, s);

    if (rXs == 0)
    {
        if (cross(pToQ, r) == 0)
        {
            //lines colinear
            //must find if overlapping
            float sDotR = dot(s, r);
            float rDotR = dot(r, r);
            float t0 = dot(pToQ, r) / rDotR;
            float t1 = t0 + (sDotR / rDotR);

            //if [t0, t1] intersects [0, 1] then
            if (sDotR < 0)
            {
                float temp = t0;
                t0 = t1;
                t1 = temp;
            }

            if (!((t0 < 0 && t1 < 0) || (t0 > 1 && t1 > 1)))
            {
                toReturn = true;
            }
        }
    }
    else
    {
        float k = cross(pToQ, s) / rXs;
        float u = cross(pToQ, r) / rXs;

        if (isInRange(k, 0, 1) && isInRange(u, 0, 1))
        {
            out = new Dim2D;
            *out = l1.pnt1 + (r * k);
            toReturn = true;
        }
    }

    return toReturn;
}