#pragma once
#include <math.h>
#include <cstdlib>
#include <assert.h>

#define PI 3.14

inline float GetRand()
{
    return ((float)rand() / (float)RAND_MAX);             //get a random number in range 0 to 1
}
inline float GetRand(float min, float max)
{
    assert(min <= max);

    float r = GetRand();
    r *= (max - min);
    return (r + min);
}
// Max inclusive
inline float GetIRand(int min, int max)
{
    int range = max - min;
    int gen = rand() % (range + 1);
    return gen + min;
}

struct Dim2D
{
    float x, y;

    bool IsOnLine(const Dim2D& start, const Dim2D& dir)
    {
        //point = start + lambda * dir
        float lambda = 0;
        lambda = (x - start.x) / dir.x;

        float test = start.y + lambda * dir.y;
        return (fabs(test - y) < 0.01f);
    }

    //Get this point in the real world, if this points origin is anchorPoint
    inline Dim2D GetRelative(const Dim2D& anchorPoint)
    {
        return { anchorPoint.x + x, anchorPoint.y + y };
    }

    inline Dim2D unit()
    {
        float mag = sqrtf(x*x + y * y);
        return { x / mag, y / mag };
    }

    //Function to rotate this point around another point, given the rotation martix
    inline void Rotate(const float (&mat)[2][2], const Dim2D around = { 0,0 })
    {
        Dim2D nPnt = { x - around.x, y - around.y };
        Dim2D rPnt = { nPnt.x * mat[0][0] + nPnt.y * mat[0][1], nPnt.x * mat[1][0] + nPnt.y * mat[1][1] };
        x = rPnt.x + around.x;
        y = rPnt.y + around.y;
    }
};

typedef Dim2D Coord, Pos, Vector, Direction;

inline Dim2D operator*(const  Dim2D d1, const float f1)
{
    return { d1.x * f1, d1.y * f1 };
}
inline Dim2D operator*(const float f1, const Dim2D d1)
{
    return Dim2D{ d1.x * f1, d1.y * f1 };
}
inline Dim2D operator- (const Dim2D d1, const Dim2D d2)
{
    return Dim2D({ d1.x - d2.x, d1.y - d2.y });
}
inline Dim2D operator/ (const Dim2D d1, const float f1)
{
    return Dim2D({ d1.x / f1, d1.y / f1 });
}
inline Dim2D operator+ (const Dim2D d1, const Dim2D d2)
{
    return Dim2D({ d1.x + d2.x, d1.y + d2.y });
}
inline bool operator==(const Dim2D d1, const Dim2D d2)
{
    return ((d1.x == d2.x) && (d1.y == d2.y));
}

inline const float cross(const Dim2D& v1, const Dim2D& v2)
{
    return (v1.x * v2.y) - (v1.y * v2.x);
}
inline const float dot(const Dim2D& v1, const Dim2D& v2)
{
    return (v1.x * v2.x) + (v1.y * v2.y);
}

struct IntCoord
{
    int x, y;
};
struct MinMax
{
    float min, max;

    int GetIntInRange()
    {
        assert(max > min);
        return (int)roundf(GetRand(min, max));
    }
    float GetInRange()
    {
        return GetRand(min, max);
    }
};

struct Timer
{
    float timeSinceStart = 0.f;
    float timeUntilEnd = 0.f;

    unsigned int updCount = 0;

    void Restart()
    {
        timeSinceStart = 0.f;
        updCount = 0;
    }

    void Start(float tToEnd)
    {
        timeUntilEnd = tToEnd;
        timeSinceStart = 0.f;
    }
    void Start(float min, float max)
    {
        Start(GetRand(min, max));
    }

    bool Tick(float elapsed)
    {
        timeSinceStart += elapsed;
        updCount += 1;
        return Tick();
    }
    bool Tick()
    {
        return (timeSinceStart >= timeUntilEnd);
    }

    bool Poll(float time)
    {
        return (timeSinceStart > time);
    }

    float GetOverflow()
    {
        return Tick() ? (timeSinceStart - timeUntilEnd) : 0;
    }
};

float GetAngleOfLine(Dim2D v);
float fMod(float arg, float div);

bool isInRange(float test, float r1, float r2);
