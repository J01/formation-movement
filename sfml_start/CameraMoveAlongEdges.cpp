// To be messed with another time


//Dim2D AttemptCameraMove(const Pos& sPoint, const Vector& toMove)
//{
//    assert(IsInBounds(sPoint));
//    Dim2D toReturn = sPoint;
//
//    // Debug variables
//    bool movedNaturally = false;
//    bool movedParallel = false;
//    Vector finalMove;
//
//    //Find if the point we should move to is in bounds
//    if (IsInBounds(sPoint + toMove))
//    {
//        toReturn = toMove + sPoint;
//        movedNaturally = true;
//        finalMove = toMove;
//    }
//    else
//    {
//        //otherwise
//        Dim2D intersect;
//        float  cBlockDist = -1;
//        size_t lineStartIdx = 0;
//
//        bool IntersectFound = false;
//
//        for (size_t i = 0; i < vertices.size(); i++)
//        {
//            //find if there is an intersection
//            Dim2D* isectTrial = nullptr;
//            LineSegment trialLine = LineSegment{ vertices[i], vertices[(i + 1) % vertices.size()] };
//
//            if (Intersect(LineSegment{ sPoint, toMove + sPoint }, trialLine, isectTrial) || trialLine.onSegment(sPoint))
//            {
//                //if an intersection is found then check if it was single point
//                if (isectTrial)
//                {
//                    //find the city block distance from the start point to the intersect
//                    float trialCBlockDist = fabs(isectTrial->x - sPoint.x) + fabs(isectTrial->y - sPoint.y);
//                    if ((trialCBlockDist < cBlockDist) || (cBlockDist == -1.f))
//                    {
//                        lineStartIdx = i;
//                        intersect = *isectTrial;
//                        IntersectFound = true;
//                    }
//                }
//            }
//        }
//
//        if (IntersectFound)
//        {
//
//            //we have now found the index of the start point of the colliding line
//            //i.e. we have found the intersected line
//            size_t lineEndIdx = (lineStartIdx + 1) % vertices.size();
//            if (LineSegment{ vertices[lineStartIdx], vertices[lineEndIdx] }.onSegment(sPoint))
//            {
//                //if on the line that it intersected with
//                //find component of toMove parallel to the line
//                //Vector line = vertices[lineEndIdx] - vertices[lineStartIdx];
//                //float lineMagSq = powf(line.x, 2) + powf(line.y, 2);
//                //float vecsDotted = dot(line, toMove);
//
//                //Vector movePar = line * (vecsDotted / lineMagSq);
//
//                Vector line = vertices[lineEndIdx] - vertices[lineStartIdx];
//                float lineAndMovDot = dot(line, toMove);
//                float lineAndLineDot = dot(line, line);
//
//                Vector movePar = line * (lineAndMovDot / lineAndLineDot);
//                Vector movePerp = toMove - movePar;
//                movePerp = movePerp * -0.2f;
//
//
//                //with the parallel move found, find if it would take the camera position beyond the end of the line
//                Pos attemptMove = sPoint + movePar;
//                if (LineSegment{ vertices[lineStartIdx], vertices[lineEndIdx] }.onSegment(attemptMove))
//                {
//                    toReturn = attemptMove + movePerp;
//                    movedParallel = true;
//                    finalMove = movePar + movePerp;
//                }
//                else
//                {
//
//                }
//            }
//            else
//            {
//                //otherwise move it to the intersection with the line
//                toReturn = intersect;
//            }
//        }
//        else
//        {
//            assert(true);
//        }
//    }
//
//    //return the final position of the camera
//    if (!IsInBounds(toReturn))
//    {
//        toReturn = sPoint;
//    }
//    return toReturn;
//}
//
//};