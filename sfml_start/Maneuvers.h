#pragma once
#include "Formation.h"
#include "Utils.h"

struct Formation;
struct Unit;

struct Manouever
{
    enum Type { FORM_UP };
    Type myType;

    Formation* fm;

    //Linked list of all units that have yet to complete the mavouever
    struct UnitLList
    {
        Unit* u = nullptr;
        UnitLList* n = nullptr;
    };
    UnitLList* rem = nullptr;

    void Begin(Type manType, Formation* fm);

    bool Update(float elapsed);

    void Empty();


    //Form up functions
    void BeginFormUp();
    bool UpdateFormUp(float elapsed);
};