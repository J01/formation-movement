#pragma once

#include "Units.h"
#include "Utils.h"
// Used to instantiate different types of weapons

struct Unit;


struct Weapon
{
    enum WeaponT { MELEE, RANGED };
    
    WeaponT myType = MELEE;
    // For both
    std::string name = "";
    MinMax damage = { 0,0 };
    int piercing = 0;                 //the amount of armour rating this ignores
    int hitBonus = 0;
    int evadeBonus = 0;
    float weight = 0.f;
    float rechargeTime = 0.f;

    // For melee
    float chargeBonus = 0.f;
    
    // For ranged
    float range = 0.f;

    sf::Texture* ptrTex = nullptr;
    sf::Texture* projTex = nullptr;

    //Function to make an attack against a unit
    // IN   :   
    // OUT  :   
    //IN/OUT:   target (the unit to be attacked)
    void Attack(Unit& attacker, Unit& target);
    void RangedAttack(Unit& attacker, Unit& target);
    void MeleeAttack(Unit& attacker, Unit& target);
};


void SetSword(Weapon* toSet);
void SetPike(Weapon* toSet);
void SetSpear(Weapon* toSet);

void SetShortbow(Weapon* toSet);
void SetLongbow(Weapon* toSet);