#include "Utils.h"

float GetAngleOfLine(Dim2D v)
{
float res = atanf(v.y / v.x);

//          |
//     Q2   |   Q1
//          |
//  -   -   -   -   -   -   >   i
//          |
//     Q3   |   Q4
//          |

//if x and y positive then Q1
//if x positive and y negative then Q4
//if x negative and y positive then Q2
//if x and y negative then Q3

if (v.x == 0)
{
    if (v.y == 0)
        res = 0;
    else if (v.y >= 0)
        res = PI / 2;
    else if (v.y <= 0)
        res = -PI / 2;
}
else if (v.y == 0)
{
    if (v.x <= 0)
        res = -PI;
    else if (v.x >= 0)
        res = 0;
}
else if (v.x <= 0 && v.y <= 0)

{
    res = -(PI - res);
}
else if (v.x <= 0 && v.y >= 0)
{
    res = (PI + res);
}

return res;
}


//Function to return an inverse cosine in the range 0 to 2pi
//  IN  :   length of hypotenuse
//          length of the known adjacent side
//  OUT :   the result of acos
//float GetACosOfLine(float hyp, float adj)
//{

//    float result = 0.f;
//
//    if (hyp)
//    {
//        
//    }
//    else
//    {
//        
//    }
//}


float fMod(float arg, float div)
{
    assert(div != 0);

    if (arg < 0)
    {
        if (div < 0)
        {
            div = -div;
            arg = -arg;
            while (arg >= div)
                arg -= div;
            arg = -arg;
        }
        else
        {
            arg = -arg;
            while (arg >= div)
                arg -= div;
            arg = -arg;
        }
    }
    else
    {
        if (div < 0)
        {
            div = -div;
            while (arg >= div)
                arg -= div;
        }
        else
        {
            while (arg >= div)
                arg -= div;
        }
    }

    return arg;
}
bool isInRange(float test, float r1, float r2)
{
    if (r2 < r1)
    {
        float ftemp;
        ftemp = r1;
        r1 = r2;
        r2 = ftemp;
    }

    return ((test >= r1) && (test <= r2));
}