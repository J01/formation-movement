#pragma once

#include <vector>

#include "Utils.h"

///////////////////////////////////////////////////////////
// Structures used to pass data to objects               //
// for use in memory allocation                          //    
///////////////////////////////////////////////////////////


// Structure used to allocate dimensions of a unit
struct FormInit
{
    IntCoord fSize;        //dimensions of formation
};
struct TeamInit
{
    int numForms;
    std::vector<FormInit> fmInit;

    //Function to initialise the team initialiser
    void Initialise(int numForms);
};