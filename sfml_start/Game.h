#pragma once

#include <vector>

#include "Utils.h"
#include "PlayerTeamController.h"
#include "ProjectileSystem.h"
#include "InitialiserStructs.h"

struct Camera;

struct Game
{
    PlayerTeam playerTeam;
    Team enemyTeam;

    ProjectileSystem projectileSys;

    Camera* camera;

    void InitialiseTeam(Team& team, TeamInit tInit);
    void InitialiseTeams(TeamInit pTeamInit, TeamInit eTeamInit);
    void Initialise(TeamInit pTeamInit, TeamInit eTeamInit);

    void CreateObjectLinks();

    void Update(float elapsed);
    void Draw();
};