#include "Team.h"

void Team::Initialise(TeamInit myInit)
{
    forms.resize(myInit.numForms);

    //for each formation
    for (int fInd = 0; fInd < myInit.numForms; fInd++)
    {
        forms[fInd].AllocateMemory(myInit.fmInit[fInd].fSize);
    }

}
void Team::Update(float elapsed)
{
    for (size_t i = 0; i < forms.size(); i++)
        forms[i].Update(elapsed);
}
void Team::Draw(Camera& cam)
{
    for (size_t i = 0; i < forms.size(); i++)
        forms[i].Draw(cam);
}