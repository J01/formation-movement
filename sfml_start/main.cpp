#include "SFML/Graphics.hpp"
#include "GameStructs.h"
#include "Formation.h"
#include "Units.h"
#include "TextureDictionary.h"
#include "PlayerTeamController.h"
#include "Camera.h"
#include "Game.h"

#include <string>
#include <iosfwd>
#include <sstream>

using namespace std;
using namespace sf;

//Texture dictionary holds all textures being used centrally without duplicates
TextureDictionary tDic;
//Camera is used to transform objects from world view to screen view
Camera cam;

int main()
{
    //Set up the camera bounds 
    cam.window.create(sf::VideoMode(1200, 800), "Military FormationsPrototype");
    cam.Init();
    cam.cBounds.Init(4);
    cam.cBounds.AddVertex(Dim2D{ -10000,-10000 });
    cam.cBounds.AddVertex(Dim2D{ -10000,10000});
    cam.cBounds.AddVertex(Dim2D{ 10000, 10000 });
    cam.cBounds.AddVertex(Dim2D{ 10000, -10000 });

    TextureDictionary tDic2;
    TextureDictionary* tDicPtr = &tDic2;
    tDic2.AddTexture("data/Unit_Swordsman_Red.png");
    TextureDictionary* tDicPtr2 = &tDic2;


    // Text for use in debugging
    sf::Text txt;
    sf::Text txt2;
    sf::Font fnt;
    bool success = fnt.loadFromFile("data/comic.ttf");
    txt.setFont(fnt);
    txt.setCharacterSize(30);
    txt.setColor(sf::Color::White);
    txt2.setFont(fnt);
    txt2.setCharacterSize(30);
    txt2.setColor(sf::Color::White);

    //Create a game structure
    Game game;


#pragma region Initialising
    TeamInit pTeamInit;
    pTeamInit.Initialise(2);

    FormInit fIn;
    fIn.fSize = { 10,10 };
    pTeamInit.fmInit[0] = fIn;
    pTeamInit.fmInit[1] = fIn;
    game.playerTeam.myTeam.Initialise(pTeamInit);

    TeamInit eTeamInit;
    eTeamInit.Initialise(3);

    fIn.fSize = { 20, 2 };
    eTeamInit.fmInit[0] = fIn;
    eTeamInit.fmInit[2] = fIn;
    fIn.fSize = { 8,3 };
    eTeamInit.fmInit[1] = fIn;
    game.enemyTeam.Initialise(eTeamInit);

    game.camera = &cam;
    game.CreateObjectLinks();
#pragma endregion
  
    //Clock manages time passing
    sf::Clock clk;
    clk.restart();
    float elapsed;

    //reserve space in the dictionary
    tDic.ReserveSpaces(10);
    TCode t = tDic.AddTexture("data/Unit_Swordsman_Red.png");
    TCode u = tDic.AddTexture("data/Unit_Swordsman_Blue.png");


    PlayerTeam& pTeam = game.playerTeam;
    pTeam.cam = &cam;

    Formation& fm = pTeam.myTeam.forms[0];
    FormationCreator fc;
    fm.tDic = &tDic;
    fc.unitTexs.push_back(t);
    fc.unitTexs.push_back(u);
    fc.fm = &fm;
    fc.SetupDimensions({ 20,20 });
    fc.SetUnitTextures();
    fm.SetFL(Dim2D{ 300,300 });
    fm.walkSpeed = 50.f;

    fc.fm = &pTeam.myTeam.forms[1];
    fc.SetupDimensions({ 0,0 });
    fc.fm = &game.enemyTeam.forms[0];
    fc.SetupDimensions({ 0,0 });
    fc.fm = &game.enemyTeam.forms[1];
    fc.SetupDimensions({ 0,0 });
    fc.fm = &game.enemyTeam.forms[2];
    fc.SetupDimensions({ 0,0 });
    //Formation& fm2 = pTeam.myTeam.forms[1];
    //FormationCreator fc2;
    //t = tDic.AddTexture("data/Unit_Cavalry_Red.png");
    //u = tDic.AddTexture("data/Unit_Cavalry_Blue.png");
    //fm2.tDic = &tDic;
    //fc2.fm = &fm2;
    //fc2.unitTexs.push_back(t);
    //fc2.unitTexs.push_back(u);
    //fc2.Setup({ 10,3 }, { 30,40 });
    //fc2.SetUnitTextures();
    //fm2.SetFL(Dim2D{ 600, 500 });
    //fm2.walkSpeed = 100.f;



    int frameCount = 0;
    float timeSinceFCUpd = 0.f;


	// Start the game loop 
	while (cam.window.isOpen())
	{
		// Process events
		sf::Event event;
        bool rClick = false;
        bool rClickReset = true;
		while (cam.window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) 
				cam.window.close();

            if (event.type == sf::Event::MouseWheelMoved)
            {
                cam.UpdateZoom(event.mouseWheel.delta);
            }
        } 

		// Clear screen
		cam.window.clear();
        elapsed = clk.getElapsedTime().asSeconds();
        clk.restart();

        //display frame rate
        timeSinceFCUpd += elapsed;
        frameCount++;
        if (timeSinceFCUpd >= 1)
        {
            stringstream str;
            str << frameCount;
            txt.setString(str.str());
            frameCount = 0;
            timeSinceFCUpd -= 1;
        }

      

        game.Update(elapsed);
        game.Draw();

        cam.window.draw(txt);


        stringstream str2;
        str2 << cam.pos.x << " , " << cam.pos.y;
        txt.setString(str2.str());
        txt.setPosition(400, 10);
        cam.window.draw(txt);

		// Draw the camera bounds and display the window
        cam.cBounds.Draw(cam);
		cam.window.display();

	}

	return EXIT_SUCCESS;
}
