#include "Maneuvers.h"
#include <assert.h>

void Manouever::Begin(Type manType, Formation* fm)
{
    myType = manType;
    this->fm = fm;

    switch (manType)
    {
        case FORM_UP:
            BeginFormUp();
            break;
        default:
            assert(false);
    }
}
bool Manouever::Update(float elapsed)
{
    switch (myType)
    {
    case FORM_UP:
        UpdateFormUp(elapsed);
        break;
    default:
        assert(false);
    }

    return !rem;
}
void Manouever::Empty()
{
    UnitLList* n;

    while (rem)
    {
        n = rem->n;
        delete rem;
        rem = n;
    }
}

void Manouever::BeginFormUp()
{
    //for every unit left alive, create a UnitLList package
    //and add it to rem

    for (size_t i = 0; i < fm->members.size(); i++)
    {
        for (size_t j = 0; j < fm->members[0].size(); j++)
        {
            if (fm->members[i][j].unit)
            {
                UnitLList* ul = new UnitLList;
                ul->u = fm->members[i][j].unit;
                ul->n = rem;
                rem = ul;
            }
        }
    }
}
bool Manouever::UpdateFormUp(float elapsed)
{
    UnitLList* curr = rem;
    UnitLList* last = nullptr;

    float moveSq = powf(fm->walkSpeed * elapsed, 2);

    while (curr)
    {
        //check if distance is already small enough
        Unit& u = *(curr->u);
        Dim2D cbDist = { 0,0 };
        cbDist = fm->members[u.formPos.x][u.formPos.y].anchor.GetRelative(fm->orient.FL) - u.GetPos();
        float realDistSq = powf(cbDist.x, 2) + powf(cbDist.y, 2);

        if (realDistSq < moveSq)
        {
            u.EnterForm();
    
            UnitLList* toDel = curr;
            curr = curr->n;
            if (last)
                last->n = curr;
            else
                rem = curr;
            delete toDel;
        }
        else
        {
            cbDist = cbDist.unit();
            u.pos = u.GetPos() + cbDist * fm->walkSpeed * elapsed;
            u.SetDirection(cbDist);

            //advance LL
            last = curr;
            curr = curr->n;
        }
    }

    return rem;
}