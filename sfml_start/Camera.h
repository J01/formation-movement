#pragma once
#include "Utils.h"
#include "SFML/Graphics.hpp"
#include "Geometry.h"

// Object to handle the camera viewpoint

// ALL DRAW COMMANDS SHOULD GO THROUGH THIS

struct CameraBounds;
struct Camera;

// Controls panning and zoom
struct CameraBounds
{
    std::vector<Dim2D> vertices;

    void Init(std::string file);
    void Init(int numVert);
    void Clear();

    void Draw(Camera& cam);

    //Used when setting the camera bounds
    //When called will return a reference to the vertex following that last returned
    //When called for the first time, will return the first element
    Dim2D& GetNextVert();
    void AddVertex(Dim2D& v);

    bool IsInBounds(Dim2D point);
    
    Dim2D AttemptCameraMove(const Pos& sPoint, const Vector& toMove);

};


static struct Camera
{
    Dim2D pos;
    Dim2D rightMove = { 1,0 }, upMove = { 0,-1 };
    
    float zoom = 1.f;
    const float MAX_ZOOM = 2.f, MIN_ZOOM = 0.25f, ZOOM_INCREMENT = 0.25f;
    const float PAN_SPEED = 100.f;


    //
    CameraBounds cBounds;

    float angle = 0.f;
    float rotMat[2][2];
    const float ROT_SPEED = PI / 4.f;

    sf::RenderWindow window;

    void Init();    
    void Draw(sf::Sprite& spr, const Dim2D& worldPos);

    //mouse position comes from space that has been transformed through the camera, so we must untransform
    Dim2D GetMousePos();

    Dim2D pntCamToWorld(Dim2D cPoint);
    Dim2D pntWorldToCam(Dim2D wPoint);

    void UpdateZoom(int delta);

    void SetAngle(float angle);

    void Pan(Dim2D toMove);
    void Pan(Dim2D dir, float elapsed);
    void PanLeft(float elapsed);
    void PanRight(float elapsed);
    void PanUp(float elapsed);
    void PanDown(float elapsed);
    void SetPanVectors();

    void Rotate(Dim2D around, float angle);
    void RotateLeft(Dim2D around, float elapsed);
    void RotateRight(Dim2D around, float elapsed);
};