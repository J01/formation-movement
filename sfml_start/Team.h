#pragma once
#include "Formation.h"
#include "InitialiserStructs.h"
#include <vector>

struct Game;
struct Camera;

// The team struct contains all the formations used by one player
struct Team
{    
   std::vector<Formation> forms;

   //pointer to the game object containing this team
   Game* game;


   void Initialise(TeamInit myInit);
   void Update(float elapsed);
   void Draw(Camera& cam);
};