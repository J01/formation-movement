#include "PlayerTeamController.h"

void PlayerTeam::Update(float elapsed)
{
    UpdateMouse(elapsed);
    myTeam.Update(elapsed);
}
void PlayerTeam::Draw(Camera& cam)
{
    myTeam.Draw(cam);
}

void PlayerTeam::UpdateMouse(float elapsed)
{
    static keyState rClickState = UP, lClickState = UP;
    static float lClickTimeDown = 0.f, rClickTimeDown = 0.f;

    const float TIME_UNTIL_HOLD = 0.1f;

    prevMousePosUI = currMousePosUI;
    currMousePosUI = Dim2D{ (float)sf::Mouse::getPosition().x, (float)sf::Mouse::getPosition().y };

    prevMousePosWorld = currMousePosWorld;
    currMousePosWorld = cam->GetMousePos();
    

    // Update the camera 
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        (*cam).PanRight(elapsed);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        (*cam).PanLeft(elapsed);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        (*cam).PanUp(elapsed);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        (*cam).PanDown(elapsed);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    {
        (*cam).RotateLeft(cam->pos, elapsed);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
    {
        (*cam).RotateRight(cam->pos, elapsed);
    }


    // Update the left click
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
    {
        //When left mouse pressed
        switch (lClickState)
        {
        case UP:
            //When the mouse was previously up
            lClickState = DOWN;
            ResolveLeftClickDown();
            lClickTimeDown = 0.f;
            break;
        case HELD:
            //When the mouse has been down for a while
            ResolveLeftClickHold();
            break;
        case DOWN:
            //When the mouse was previously down
            lClickTimeDown += elapsed;
            lClickState = lClickTimeDown < TIME_UNTIL_HOLD ? DOWN : HELD;
            break;
        }
    }
    else
    {
        //When left mouse not pressed
        switch (lClickState)
        {
        case UP:
            // When the mouse was previously up
            break;
        case HELD:
        case DOWN:
            //When the mouse was previously down
            //i.e. on mouse release
            lClickState = UP;
            lClickTimeDown = 0.f;
            ResolveLeftClickUp();
            break;
        }
    }

    // Update the right click
    if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
    {
        //When right mouse pressed
        switch (rClickState)
        {
        case UP:
            //When the mouse was previously up
            rClickState = DOWN;
            ResolveRightClickDown();
            rClickTimeDown = 0.f;
            break;
        case HELD:
            //When the mouse has been down for a while
            ResolveRightClickHold();
            break;
        case DOWN:
            //When the mouse was previously down
            rClickTimeDown += elapsed;
            rClickState = rClickTimeDown < TIME_UNTIL_HOLD ? DOWN : HELD;
            break;
        }
    }
    else
    {
        //When right mouse not pressed
        switch (rClickState)
        {
        case UP:
            // When the mouse was previously up
            break;
        case HELD:
        case DOWN:
            //When the mouse was previously down
            //i.e. on mouse release
            rClickState = UP;
            rClickTimeDown = 0.f;
            ResolveRightClickUp();
            break;
        }
    }
   
}

void PlayerTeam::ResolveLeftClickDown()
{
    // WILL ONLY BE CALLED IF THE LEFT CLICK HAS JUST BEEN MADE
    lClickDownPos = currMousePosWorld;
}
void PlayerTeam::ResolveLeftClickUp()
{
    //WILL ONLY BE CALLED IF THE LEFT CLICK HAS JUST BEEN RELEASED
    Dim2D mousePos = currMousePosWorld;

    //Check all formations in the team, and check which, if any has been clicked
    currForm = nullptr;

    for (size_t i = 0; i < myTeam.forms.size()&& !currForm; i++)
    {
        Formation& fm = myTeam.forms[i];
        if (IsFormationClicked(mousePos, fm))
        {
            currForm = &fm;
        }
    }
}
void PlayerTeam::ResolveLeftClickHold()
{
    //allow players to pan the camera using the mouse as long as there is no formation selected
    if (!currForm)
    {
        //camera should move opposite to the direction of movement of the mouse
        cam->Pan(prevMousePosWorld - currMousePosWorld);
        currMousePosWorld = cam->GetMousePos();
    }
    else
    {

    }
}


void PlayerTeam::ResolveRightClickDown()
{
    rClickDownPos = currMousePosWorld;
}
void PlayerTeam::ResolveRightClickUp()
{
    // This is really gonna take some work
    if (currForm)
    {
        currForm->ClearActions();
        currForm->BeginMarch(rClickDownPos, currForm->orient);
    }

}
void PlayerTeam::ResolveRightClickHold()
{
    //if no formation selected, then begin rotate
    if (!currForm)
    {
        //the point to rotate around is rClickDownPos
        //find difference in the x positions of the UI mouse positions
        int xDiff = (int)currMousePosUI.x - (int)prevMousePosUI.x;
        const float DIST_PER_RAD = 30.f;
        float ang = xDiff / DIST_PER_RAD;

        cam->Rotate(rClickDownPos, ang);
    }
    else
    {

    }
}


bool PlayerTeam::IsFormationClicked(Dim2D clickPos, Formation& fm)
{
    //Find if the click position is in the formation bounds
    //solve via simultaneous equations with a matrix
    
    // d is the vector from FL to the click point
    Dim2D d = clickPos - fm.orient.FL;
    // b is the vector from FL to FR
    // c is the vector from FL to BL
    Dim2D b = fm.orient.FR - fm.orient.FL;
    Dim2D c = fm.orient.BL - fm.orient.FL;

    //Create the determinant of the sim eq matrix
    //          [   b.x     c.x     ]
    //  M   =   [                   ]
    //          [   b.y     c.y     ]
    float matDet = 1.f / ((b.x * c.y) - (c.x * b.y));
    //                          [   c.y     -c.x    ]
    //  M^-1    =   matDet *    [                   ]
    //                          [   -b.y    b.x     ]
    float bMult = matDet * ((c.y * d.x) - (c.x * d.y));
    if (0 <= bMult && bMult <= 1)
    {
        float cMult = matDet * (-(b.y * d.x) + (b.x * d.y));
        if (0 <= cMult && cMult <= 1)
        {
            return true;
        }
    }

    return false;
}