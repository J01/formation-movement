#include "Units.h"

bool Unit::Update(float elapsed)
{
    attTimer.Tick(elapsed);

    return false;
}

bool Unit::TakeDamage(int damAmount)
{
    if (health <= damAmount)
    {
        //Formation.remove(FormPos)
        return true;
    }
    return false;
}
void Unit::AddFatigue(float fatigue)
{
    currFatigue += fatigue;
    if (currFatigue > myForm->memberMaxFatigue)
        fatigued = true;
}
void Unit::DecayFatigue(float decayTime)
{
    currFatigue -= (decayTime / 10);
    if (fatigued)
        if (currFatigue < myForm->memberFatigueReset)
            fatigued = false;
}

void Unit::Draw(Camera& cam)
{
    cam.Draw(spr, GetPos());
}
Dim2D Unit::GetPos()
{
    assert(myForm);
    assert(this);
    return inForm ? (myForm->members[formPos.x][formPos.y].anchor) : pos;
}
void Unit::EnterForm()
{
    if (!inForm)
        myForm->UnitsScattered--;
    inForm = true;
    SetDirection(myForm->orient.facingDir);
}
void Unit::LeaveForm()
{
    if (inForm)
        myForm->UnitsScattered++;
    inForm = false;
    pos = GetPos();
}

void Unit::SetAngle(float ang)
{
    spr.setRotation(ang);
}
void Unit::SetDirection(Dim2D dir)
{
    float ang = GetAngleOfLine(dir);
    spr.setRotation(ang * 180.f / PI + 90.f);
}

//Attack takes place
//  Find if hit
//      Based on attacking units proficiency
//      Take into account evasion of opponent and the equipment
//  Find damage
//      Based on weapon min max damage
//      Amount to block found via armour vs max damage
//          Ignore piercing of blocked damage
//  Find death
void Attack(Unit& attacker, Unit& defender)
{
    //find if hit
    float hitChance = 0.5f;


    hitChance += (attacker.myForm->memberHSkill - defender.myForm->memberDSkill) / attacker.myForm->memberHSkill;
    if (hitChance >= 0.98f)
        hitChance = 0.98f;
    else if (hitChance <= 0.02f)
        hitChance = 0.02f;


    int resist = 0;
    if (defender.myForm->body)
    {
        resist = defender.myForm->body->resistance.GetInRange();
        //find the amount of damage that can be blocked

        resist - attacker.myForm->rightArm->piercing;
        //take into account piercing
        if (resist <= 0)
        {
            resist = 0;
        }
    }

    int damage = attacker.myForm->rightArm->damage.GetInRange();
    damage -= resist;
    //defender.takedamage(damage)
}

