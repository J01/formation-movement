#pragma once
#include "Utils.h"

//Header file containing and explaining simple geometric operations for use in 2D programs


//Enumerates various types of point orientation
//Taking 3 points we can find the orientation by comparing the gradients
//of vectors between the points
// With variables
// a : gradient between first and second
// b : gradient between second and third
// If a and b are the same, the points are colinear, i.e. part of the same vector
// if a > b, the orientation if clockwise
//      2 -- 3
//    /
//  1
// Seen above, it is clear that if the gradient is lower, it appears in a clockwise pattern
// if a < b then the orientation is anticlockwise, logically following
const enum Orientation { CLOCKWISE = 0, ANTICLOCKWISE = 1, COLINEAR = 2 };
//Function to find the orientation of 3 points
// IN   :   p1  , the first point
//          p2  , the second point
//          p3  , the third point
// OUT  :   orientation of given points, as enum
const Orientation GetOrientation(const Dim2D& p1, const Dim2D& p2, const Dim2D& p3);


//A line segment is a bounded line going from one position vector to another
// This acts only as a small wrapper for the data, and should not be extended to have
// much functionality
// This should be done instead with a struct, BoundedLine
struct LineSegment
{
    Pos pnt1, pnt2;

    //Function to find if a point is on a line segment
    // IN   :   pnt , the point to try
    // OUT  :   bool, true if on the line
    const bool onSegment(const Pos& pnt);
};

//A function to find if two given line segments intersect, using orientation of points
//  IN  : l1    ,   the first line segment
//        l2    ,   the second line segment
//  OUT : bool  ,   true if the lines intersect
const bool Intersect(const LineSegment& l1, const LineSegment& l2);

//A function to find if two given line segments intersect, also finding the point of
//intersection, and returning through parameters
//  IN  :   l1  ,   the first line segment
//          l2  ,   the second line segment
//  OUT :   out ,   return variable for the point of intersection, pointer to position. If null when returned, no single point intersection found
//  post:   if out is not nullptr, it points to the single point intersection of the two line segments
const bool Intersect(const LineSegment& l1, const LineSegment& l2, Pos*& out);

