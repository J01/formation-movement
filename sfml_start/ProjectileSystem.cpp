#include "ProjectileSystem.h"


void Projectile::Create(const sf::Texture* tex, const Dim2D& size, const Dim2D& target, const Dim2D& start, const float speed)
{
    spr.setTexture(*tex, true);
    spr.setScale(size.x / tex->getSize().x, size.y / tex->getSize().y);
    
    //angle the projectile to face the direction of travel
    Dim2D ln = target - start;
    const float rotOffset = 0.f;
    spr.setRotation(GetAngleOfLine(ln) + rotOffset);

    pos = start; 
    
    //find the time remaining and the move per second
    float travLen = sqrtf(powf(ln.x, 2) + powf(ln.y, 2));
    timeRemaining = travLen / speed;
    movePerSec = ln / speed;
}
bool Projectile::UpdateMove(const float elapsed)
{
    bool finished = false;

    if (elapsed >= timeRemaining)
    {
        timeRemaining = elapsed;
        finished = true;
    }
    
    pos = movePerSec * timeRemaining;
    return finished;
}
bool Projectile::UpdateHold(const float elapsed)
{
    timeRemaining -= elapsed;
    return timeRemaining > 0;
}


void ProjectileSystem::Initialise()
{
    lstFree = &membs[0];

    for (size_t i = 0; i < numProj - 1; i++)
    {
        membs[i].next = &membs[i + 1];
    }
}
void ProjectileSystem::Update(float elapsed)
{
    //Update all the projectiles in the moving and ground lists
    //ignore free because they're not doing anything

    Projectile* curr = lstMoving;
    Projectile* last = nullptr;

    while (curr)
    {
        bool finished = curr->UpdateMove(elapsed);
        if (finished)
        {
            EndMove(curr, last);

            //now check for collisions with the enemy team

            //if collided remove from ground list (easy as is at front)
        }
        last = curr;
        curr = curr->next;
    }

    curr = lstGround;
    last = nullptr;
    while (curr)
    {
        bool finished = curr->UpdateHold(elapsed);
        if (finished)
        {
            EndGround(curr, last);
        }
        last = curr;
        curr = curr->next;
    }
}
void ProjectileSystem::Draw(Camera& cam)
{
    //Reference to screen object can be inserted here
    Projectile* curr = lstMoving;
    while (curr)
    {
        // [ account for screen ]
        cam.Draw(curr->spr, curr->pos);
        curr = curr->next;
    }

    curr = lstGround;
    while (curr)
    {
        // [ account for screen ]
        cam.Draw(curr->spr, curr->pos);
        curr = curr->next;
    }
}
void ProjectileSystem::EndMove(Projectile* curr, Projectile* last)
{
    if (last)
        last->next = curr->next;
    else
        lstMoving = curr->next;

    AddToGround(curr);
}
void ProjectileSystem::EndGround(Projectile* curr, Projectile* last)
{
    RemoveFromGround(curr, last);
}


void ProjectileSystem::AddToGround(Projectile* curr)
{
    if (lstGroundEnd)
    {
        lstGroundEnd->next = curr;
    }
    else
    {
        lstGround = curr;
        lstGroundEnd = curr;
    }
    curr->next = nullptr;
}
void ProjectileSystem::RemoveFromGround(Projectile* curr, Projectile* last)
{
    if (last)
        last->next = curr->next;
    else
        lstGround = last;

    if (!curr->next)
        lstGroundEnd = last;

    curr = lstFree;
    lstFree = curr;
}

Projectile* ProjectileSystem::GetProjectile()
{
    Projectile* toReturn = nullptr;

    //if there are none free, and there are on the ground, move one from the ground list 
    if (!lstFree && lstGround)
    {
        RemoveFromGround(lstGround, nullptr);
    }

    if (lstFree)
    {
        toReturn = lstFree;
        toReturn->timeRemaining = 0;
        lstFree = toReturn->next;
        
        toReturn->next = lstMoving;
        lstMoving = toReturn;
    }
    
    return toReturn;
  
}