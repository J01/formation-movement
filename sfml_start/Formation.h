#pragma once

#include "SFML/Graphics.hpp"
#include "Units.h"
#include "Utils.h"
#include "Maneuvers.h"
#include "TextureDictionary.h"
#include "Camera.h"
#include <vector>

struct Unit;
struct FormSlot
{
    Unit* unit;
    Dim2D anchor;                                       //relative to FL
};


struct Weapon;
struct Shield;
struct Armour;

struct Formation;
struct Manouever;

struct Action
{
    enum ActionType {MANOUEVER, MARCHING, CHARGING, TURNING, HOLDING};
    ActionType myType;

    float timeRemaining = 0;
    //IF MARCHING, then distance to march
    //IF TURNING , then degree to turn
    //IF HOLDING , then time to hold
    float magnitude = 0;

    //IF MARCHING, then location to move to
    //IF CHARGING, then position of formation in enemy team object
    //IF TURNING , then pivot position
    Dim2D movT;

    // pointer to the next action
    Action* nextAct = nullptr;
};

struct FormationCreator
{
    Formation* fm;

    //A vector of pointers to the textures used to display the units
    std::vector<TCode> unitTexs;
    void SetUnitTextures();

    //Function to setup the formation, given the ranks and the size of the unit
    void SetupDimensions(Dim2D unitSize);

    //Set the equipment of the member units
    // IN   :   armour ptr, weapon ptr, shield ptr
    // OUT  :   
    //IN/OUT:   
    void SetEquip(Armour* armour = nullptr, Weapon* weapon = nullptr, Shield* shield = nullptr);
    //Function to setup a given unit 
    void SetupUnit(Unit* unit);
};

struct FormOrientation
{
    Dim2D facingDir;
    Dim2D FL, FR, BL, BR, C;
};


struct Formation
{
    std::vector<std::vector<FormSlot>> members;

    //MEMBER STATS
#pragma region MemberStats
    Weapon* rightArm = nullptr;
    Shield* leftArm = nullptr;
    Armour* body = nullptr;

    int memberHSkill = 0;
    int memberBasicHSkill = 0;
    int memberDSkill = 0;
    int memberBasicDSkill = 0;

    int memberBasicHealth = 1;

    float memberMaxFatigue = 1.f;
    float memberFatigueReset = 0.5f;

    float memberAtkDelay = 1.f;

    MinMax chargeSpeed = { 0.f, 0.01f };
    float walkSpeed = 0.f;
#pragma endregion

    float GetEquipWeight();
    int GetEquipHitSkill();
    int GetEquipDefendSkill();


    //ORIENTATION
    FormOrientation orient;
    Dim2D size;
    Dim2D unitSize;
    Dim2D cellSize;
    void SetFL(Dim2D& pos);

    TextureDictionary* tDic = nullptr;
    

    //ACTION LOGIC
    enum FormState { IN_FORM, SCATTERED };
    FormState currState = IN_FORM;
    Manouever* myMan = nullptr;

    int UnitsScattered = 0;

    Action* myAction;
    void ClearActions();                            //Clear all commands from the action queue
    void SetNoCommands();                           //Clear all commands and set to hold
    void NextAction();                              //Advance the action queue

    //Function to create an action, add it to the end of the queue
    //and return a pointer to it
    Action* QueueAction();

    void AddMarch(float distance)
    {
        //find last action in the queue
        Action* actPtr = myAction;
        while (actPtr->nextAct)
            actPtr = actPtr->nextAct;
        actPtr->nextAct = new Action;
        
        Action& newAct = (*actPtr->nextAct);
        newAct.magnitude = distance;
        newAct.myType = Action::MARCHING;
    }
    void AddHold(float time)
    {
        //find last action in the queue
        Action* actPtr = myAction;
        while (actPtr->nextAct)
            actPtr = actPtr->nextAct;
        actPtr->nextAct = new Action;

        Action& newAct = (*actPtr->nextAct);
        newAct.magnitude = time;
        newAct.myType = Action::HOLDING;
    }

    bool Update(float elapsed);

    bool TurnToFace(Dim2D target, FormOrientation& o);
    float CalculateTurn(Dim2D target, FormOrientation& o);

    void BeginMarch(Dim2D target, FormOrientation& o);
    bool ForwardMarch(float elapsed);

    void BeginTurn(float magnitude);
    bool ContinueTurn(float elapsed);

    void BeginFormUp();
    bool UpdateManouever(float elapsed);

    bool AllUnitsInForm();

    bool Hold(float elapsed);

    void Condense();

   
    void Draw(Camera& cam);


    //bool RecieveOrder(OrderType oType, Formation* oForm);
    //bool RecieveOrder(OrderType oType, Dim2D loc, Dim2D forward);
    //bool RecieveOrder(OrderType oType);

    // Function to resize vectors ready for formation setup
    // Memory allocation so that no data is later moved
    void AllocateMemory(IntCoord formSize)
    {
        std::vector<FormSlot> fsVec;
        fsVec.resize(formSize.y);
        members.resize(formSize.x, fsVec);
    }
};