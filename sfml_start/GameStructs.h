#pragma once
#include <math.h>

struct CartVec
{
    float i = 0, j = 0;

    CartVec operator +(CartVec& other)
    {
        return { i + other.i, j + other.j };
    }
    CartVec operator -(CartVec& other)
    {
        return { i - other.i, j - other.j };
    }
    
    void Normalise()
    {
        //expensive square root function isn't brilliant
        float mag = 1 / sqrtf(i*i + j * j);
        i = i * mag;
        j = j * mag;
    }
    CartVec GetPerp()
    {
        return { j, -i };
    }

    float dotProd(CartVec& other)
    {
        float result = 0;
        result += i * other.i;
        result += j * other.j;
        return result;
    }

    inline CartVec operator *(float& other)
    {
        return { other * i, other * j };
    }
    inline CartVec operator *(int& other)
    {
        return { i * other, j * other };
    }
};

struct Particle
{
    CartVec v = { 0,0 };
    CartVec s = { 0, 0 };

    float m = 1;
    float e = 0.9;

    float r = 1;

    void Update(float elapsed);
};

bool CheckCollision(Particle& p1, Particle& p2);
void Collide(Particle& p1, Particle& p2);


