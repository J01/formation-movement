#include "Camera.h"
#include <limits>

void CameraBounds::Init(std::string file)
{

}
void CameraBounds::Init(int numVert)
{
    vertices.reserve(numVert);
    Dim2D d = { 0,0 };
    for (size_t i = 0; i < numVert; i++)
    {
        vertices.push_back(d);
    }
}
void CameraBounds::Clear()
{
    vertices.clear();
}
Dim2D& CameraBounds::GetNextVert()
{
    static int currVert = -1;
    currVert++;
    if (currVert >= vertices.size())
    {
        currVert = 0;
    }
    return vertices[currVert];
}
void CameraBounds::AddVertex(Dim2D& v)
{
    Dim2D& vRef = GetNextVert();
    vRef = Dim2D{ v.x, v.y };
}

bool CameraBounds::IsInBounds(Dim2D point)
{
    //can check if within the polygon by drawing a line to the direct right, and seeing how many intersections
    bool inBounds = false;
    bool exit = false;
   
    //find the y component of point
    
    //for each line
    for (size_t i = 0; i < vertices.size() && !exit; i++)
    {
        Dim2D& v1 = vertices[i], v2 = vertices[(i + 1) % vertices.size()];

        if (v2.y - v1.y != 0)
        {
            //cast a line of infinite length rightwards from the given point
            // find lines that contain the y value
            if (isInRange(point.y, v1.y, v2.y))
            {
                //find the corresponding x value to that y
                float mult = (point.y - v1.y) / (v2.y - v1.y);
                float x = v1.x + (mult * (v2.x - v1.x));

                //if on the line then must be in bounds; all lines are in bounds
                //if near enough equal
                if (trunc(point.x * 1024) == trunc(x * 1024))
                {
                    inBounds = true;
                    exit = true;
                }
                else if (point.x < x)
                {
                    inBounds = !inBounds;
                }
            }
        }
    }
    return inBounds;
}
void CameraBounds::Draw(Camera& cam)
{
    for (size_t i = 0; i < vertices.size(); i++)
    {
        sf::VertexArray va;
        Dim2D pnt1 = cam.pntWorldToCam(vertices[i]);
        Dim2D pnt2 = cam.pntWorldToCam(vertices[(i + 1) % vertices.size()]);
        va.resize(2);
        va[0].color = sf::Color::Red;
        va[1].color = sf::Color::Red;
        va.append(sf::Vector2f{ pnt1.x, pnt1.y });
        va.append(sf::Vector2f{ pnt2.x, pnt2.y });
        va.setPrimitiveType(sf::Lines);
        cam.window.draw(va);
    }
}    
Dim2D CameraBounds::AttemptCameraMove(const Pos& sPoint, const Vector& toMove)
{
    assert(IsInBounds(sPoint));
    Dim2D toReturn = sPoint;

    // Debug variables
    bool movedNaturally = false;
    bool movedParallel = false;
    Vector finalMove;

    //Find if the point we should move to is in bounds
    if (IsInBounds(sPoint + toMove))
    {
        toReturn = toMove + sPoint;
        movedNaturally = true;
        finalMove = toMove;
    }

    return toReturn;
}



void Camera::Init()
{
    pos = Dim2D{ window.getSize().x / 2.f, window.getSize().y / 2.f };
    SetAngle(0);
}
void Camera::Draw(sf::Sprite& sprite, const Dim2D& worldPos)
{
    //Need to handle scaling of the sprite and repositioning relative to the screen

    //using matrices
    //   rotate * scale * translate

    //position the sprite
    Dim2D drawLocation = pntWorldToCam(worldPos);

    sprite.setPosition(drawLocation.x, drawLocation.y);


    //just change the sprite scale
    sf::Vector2f origScale = sprite.getScale();
    sprite.setScale(origScale.x * zoom, origScale.y * zoom);
    float origAngle = sprite.getRotation();
    sprite.setRotation(sprite.getRotation() + (angle * (180.f / PI)));
    window.draw(sprite);

    sprite.setScale(origScale);
    sprite.setRotation(origAngle);
}

Dim2D Camera::GetMousePos()
{
    sf::Vector2i mPosRaw = sf::Mouse::getPosition(window);
    ////det will always be one
    ////  a   b   ->  d   -b
    ////  c   d       -c  a
    //Dim2D mPos;
    ////mPos.x = d * mPosRaw.x - b * mPosRaw.y
    ////mPos.y = -c * mPosRaw.x + a * mPosRaw.y
    //mPos.x = rotMat[1][1] * mPosRaw.x - rotMat[0][1] * mPosRaw.y;
    //mPos.y = -rotMat[1][0] * mPosRaw.x + rotMat[0][0] * mPosRaw.y;

    ////scale according to zoom
    //mPos = mPos * (1 / zoom);

    //mPos.x += pos.x;
    //mPos.y += pos.y;
    //return mPos;
    return pntCamToWorld(Dim2D{ (float)mPosRaw.x ,(float)mPosRaw.y });
}
Dim2D Camera::pntCamToWorld(Dim2D cPoint)
{
    //remove the drawing thing
    Dim2D unScreened = cPoint - Dim2D{ (float)window.getSize().x / 2, (float)window.getSize().y / 2 };

    //cos(a)    -sin(a) ->  cos(a)  sin(a)
    //sin(a)    cos(a)      -sin(a) cos(a)
    Dim2D wldPnt;
    wldPnt.x = unScreened.x * rotMat[0][0] + unScreened.y * rotMat[1][0];
    wldPnt.y = unScreened.x * rotMat[0][1] + unScreened.y * rotMat[1][1];

    wldPnt = wldPnt * (1 / zoom);
    return wldPnt + pos;
}
Dim2D Camera::pntWorldToCam(Dim2D cPoint)
{
    //transform and scale
    Dim2D centreToObj = (cPoint - pos) * zoom;
    Dim2D newCToObj = centreToObj;
    //rotate
    newCToObj.x = centreToObj.x * rotMat[0][0] + centreToObj.y * rotMat[0][1];
    newCToObj.y = centreToObj.x * rotMat[1][0] + centreToObj.y * rotMat[1][1];


    return Dim2D{ (float)(window.getSize().x / 2), (float)(window.getSize().y / 2) } +newCToObj;
}


void Camera::UpdateZoom(int delta)
{
    float startZoom = zoom;
    Dim2D mousePos = GetMousePos();

    zoom += ZOOM_INCREMENT * delta;
    if (zoom < MIN_ZOOM)
        zoom = MIN_ZOOM;
    else if (zoom > MAX_ZOOM)
        zoom = MAX_ZOOM;

    //zoom into the given point, by scaling around the mousePosition rather than the centre
    //use mousePos as the origin
    Dim2D camToMouse = mousePos - this->pos;
    camToMouse = camToMouse * (startZoom / zoom);
    pos = mousePos - camToMouse;
}

void Camera::SetAngle(float angle)
{
    this->angle = angle;

    //  [0][0]  [0][1]
    //  [1][0]  [1][1]

    rotMat[0][0] = cosf(angle);
    rotMat[1][1] = rotMat[0][0];
    rotMat[1][0] = sinf(angle);
    rotMat[0][1] = -rotMat[1][0];
}

void Camera::Pan(Dim2D toMove)
{
    //Dim2D trialPos = pos + toMove;
    //if (cBounds.IsInBounds(trialPos))
    //{
    //    pos = trialPos;
    //}
    pos = cBounds.AttemptCameraMove(pos, toMove);
}
void Camera::Pan(Dim2D dir, float elapsed)
{
    Pan(dir * PAN_SPEED *  elapsed);
}
void Camera::PanRight(float elapsed)
{
    Pan(rightMove, elapsed);
}
void Camera::PanLeft(float elapsed)
{
    Pan(Dim2D{ -rightMove.x, -rightMove.y }, elapsed);
}
void Camera::PanUp(float elapsed)
{
    Pan(upMove, elapsed);
}
void Camera::PanDown(float elapsed)
{
    Pan(Dim2D{ -upMove.x, -upMove.y }, elapsed);
}
void Camera::SetPanVectors()
{
    rightMove = Dim2D{ cosf(-angle), sinf(-angle) };
    upMove = Dim2D{ cosf(-(angle + PI / 2)), sinf(-(angle + PI / 2)) };
}

void Camera::Rotate(Dim2D around, float angle)
{
    SetAngle(this->angle + angle);

    // move around the pivot
    Dim2D recentred = pos - around;
    Dim2D rotated;
    rotated.x = cosf(angle) * recentred.x -sinf(-angle) * recentred.y;
    rotated.y = sinf(-angle) * recentred.x + cosf(angle) * recentred.y;

    Pos newPos = rotated + around;
    Vector toMove = newPos - pos;
    Pan(toMove);
    SetPanVectors();
}
void Camera::RotateLeft(Dim2D around, float elapsed)
{
    Rotate(around, elapsed * -ROT_SPEED);
}
void Camera::RotateRight(Dim2D around, float elapsed)
{
    Rotate(around, elapsed * ROT_SPEED);
}