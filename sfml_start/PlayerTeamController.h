#pragma once
#include "Team.h"

struct Camera;

struct PlayerTeam
{
    Team myTeam;
    Camera* cam = nullptr;

    Dim2D currMousePosWorld;
    Dim2D prevMousePosWorld;
    Dim2D currMousePosUI;
    Dim2D prevMousePosUI;

    void Update(float elapsed);
    void Draw(Camera& cam);
    
    Formation* currForm = nullptr;

    enum keyState {DOWN, HELD, UP};

    Dim2D lClickDownPos;
    Dim2D rClickDownPos;

    void UpdateMouse(float elapsed);

    void ResolveLeftClickDown();
    void ResolveLeftClickUp();
    void ResolveLeftClickHold();

    void ResolveRightClickUp();
    void ResolveRightClickDown();
    void ResolveRightClickHold();

    bool IsFormationClicked(Dim2D clickPos, Formation& fm);

};