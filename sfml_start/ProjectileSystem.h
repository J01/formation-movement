#pragma once

#include "Units.h"
#include "Formation.h"

#include <assert.h>

struct Camera;


struct Projectile
{
    float timeRemaining = 0.f;
    Dim2D movePerSec = { 0,0 };
    Dim2D pos = { 0,0 };

    sf::Texture* ptrTex = nullptr;
    sf::Sprite spr;

    Projectile* next;

    void Create(const sf::Texture*, const Dim2D& size, const Dim2D& target, const Dim2D& start, const float speed);
    bool UpdateMove(const float elapsed);
    bool UpdateHold(const float elapsed);
};


const size_t numProj = 500;
const float groundTime = 10.f;

//An object handling all the in game projectiles
struct ProjectileSystem
{
    Projectile membs[numProj];
    
    Projectile* lstMoving = nullptr;            //LIFO
    Projectile* lstFree = nullptr;              //LIFO

    Projectile* lstGround = nullptr;            //FIFO
    Projectile* lstGroundEnd = nullptr;
    
    void Initialise();
    void Update(float elapsed);
    void Draw(Camera& cam);

    void EndMove(Projectile* curr, Projectile* last);
    void EndGround(Projectile* curr, Projectile* last);

    //The ground list acts like a queue 
    void AddToGround(Projectile* curr);
    void RemoveFromGround(Projectile* curr, Projectile* last);
    
    Projectile* GetProjectile();
};