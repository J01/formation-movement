#include "Formation.h"

const bool SHOULD_ROTATE = true;

void FormationCreator::SetupDimensions(Dim2D unitSize)
{
    fm->unitSize = unitSize;

    fm->cellSize.x = unitSize.x * 1.1f;
    fm->cellSize.y = unitSize.y * 1.1f;


    //start at (0,0)
    Dim2D currLoc = { 0,0 };
    for (size_t i = 0; i < fm->members.size(); i++)
    {
        for (size_t j = 0; j < fm->members[0].size(); j++)
        {
            FormSlot fs;
            fm->members[i][j].unit = new Unit;
            SetupUnit(fm->members[i][j].unit);

            fm->members[i][j].anchor.x = (fm->cellSize.x * i) + (0.5f * fm->cellSize.x);
            fm->members[i][j].anchor.y = (fm->cellSize.y * j) + (0.5f * fm->cellSize.y);

            fm->members[i][j].unit->formPos = IntCoord({ static_cast<int>(i),static_cast<int>(j) });
        }
    }

    fm->myAction = new Action;
    fm->myAction->myType = Action::HOLDING;

    fm->orient.FL = { 0,0 };
    fm->orient.FR = { (fm->members.size()) * fm->cellSize.x, 0 };
    fm->orient.BR = { (fm->members.size()) * fm->cellSize.x, (fm->members[0].size()) * fm->cellSize.y };
    fm->orient.BL = { 0, (fm->members[0].size()) * fm->cellSize.y };

    fm->orient.C = { fm->orient.BR.x / 2, fm->orient.BR.y / 2 };

    fm->orient.facingDir = (fm->orient.FL - fm->orient.BL).unit();

    fm->size = fm->orient.BR;
}
void FormationCreator::SetUnitTextures()
{
    for (size_t i = 0; i < fm->members.size(); i++)
    {
        for (size_t j = 0; j < fm->members[0].size(); j++)
        {
            int texNum = GetIRand(0, unitTexs.size() - 1);
            const sf::Texture& t = fm->tDic->Get(unitTexs[texNum]);            
            sf::Sprite& s = fm->members[i][j].unit->spr;

            s.setTexture(t);
            s.setScale(fm->unitSize.x / t.getSize().x, fm->unitSize.y / t.getSize().y);
            s.setOrigin(t.getSize().x / 2, t.getSize().y / 2);
        }
    }
}
void FormationCreator::SetEquip(Armour* armour, Weapon* weapon, Shield* shield)
{
    fm->body = armour;
    fm->rightArm = weapon;
    fm->leftArm = shield;

    fm->memberHSkill = fm->GetEquipHitSkill();
    fm->memberDSkill = fm->GetEquipDefendSkill();
}
void FormationCreator::SetupUnit(Unit* unit)
{
    unit->chargeSpeed = fm->chargeSpeed.GetInRange();

    unit->myForm = fm;
    unit->health = fm->memberBasicHealth;

    unit->attTimer.Start(fm->memberAtkDelay);
}




//FORMATION DECLARATIONS                //
float Formation::GetEquipWeight()
{
    assert(rightArm);
    float equipWeight = rightArm->weight;
    if (body)
        equipWeight += body->weight;
    if (leftArm)
        equipWeight += leftArm->weight;
    return equipWeight;
}
int Formation::GetEquipHitSkill()
{
    assert(rightArm);

    float hSkill = memberBasicHSkill + rightArm->hitBonus;
    if (body)
        hSkill += body->hitBonus;
    if (leftArm)
        hSkill += leftArm->hitBonus;

    return hSkill;
}
int Formation::GetEquipDefendSkill()
{
    assert(rightArm);

    float dSkill = memberBasicDSkill + rightArm->evadeBonus;
    if (body)
        dSkill += body->evadeBonus;
    if (leftArm)
        dSkill += leftArm->evadeBonus;

    return dSkill;
}

void Formation::SetFL(Dim2D& pos)
{
    Dim2D diff = pos - orient.FL;

    orient.FL = orient.FL + diff;
    orient.FR = orient.FR + diff;
    orient.BL = orient.BL + diff;
    orient.BR = orient.BR + diff;
    orient.C = orient.C + diff;
    for (size_t i = 0; i<members.size();i++)
        for (size_t j = 0; j < members[0].size(); j++)
        {
            members[i][j].anchor = members[i][j].anchor + diff;
        }
}

bool Formation::Update(float elapsed)
{
    bool finished = false;


    switch (myAction->myType)
    {
    case Action::HOLDING:
        finished = Hold(elapsed);
        break;
    case Action::TURNING:
        if (AllUnitsInForm())
            finished = ContinueTurn(elapsed);
        else
            SetNoCommands();
        break;
    case Action::MARCHING:
        if (AllUnitsInForm())
            finished = ForwardMarch(elapsed);
        else
            SetNoCommands();
        break;
    case Action::MANOUEVER:
        finished = UpdateManouever(elapsed);
        break;
    }

    for (unsigned int i = 0; i < members.size(); i++)
    {
        for (unsigned int j = 0; j < members[0].size(); j++)
        {
            if (members[i][j].unit)
                members[i][j].unit->Update(elapsed);
        }
    }

    if (finished)
    {
        NextAction();
    }

    return true;
}
void Formation::Condense()
{
    IntCoord sz = { members.size(), members[0].size() };

    for (unsigned int j = 0; j < sz.y - 1; j++)
    {
        for (unsigned int i = 0; i < sz.x; i++)
        {

            if (!members[i][j].unit)
            {
                bool found = false;
                //check directly backwards
                for (unsigned int y = j + 1; (y < sz.y && !found); y++)
                {
                    if (members[i][y].unit)
                    {
                        members[i][j].unit = members[i][y].unit;
                        members[i][y].unit = nullptr;
                    }
                }

            }

        }
    }
}                                //move columns forward to reinforce further lines

//Function to calculate the angle that a line must be turned such that the normal to the line passes through a given point
//  IN  :   the point to end facing towards
//  OUT :   the shortest angle to turn the line
//IN/OUT:   
float Formation::CalculateTurn(Dim2D T, FormOrientation& o)
{
    //Find the pivot and the distance from the pivot to the target
    bool FLisPiv = false;
    float distFLtoT = (powf(T.x - o.FL.x, 2) + powf(T.y - o.FL.y, 2));
    float distFRtoT = (powf(T.x - o.FR.x, 2) + powf(T.y - o.FR.y, 2));
    float distPivToT = 0;
    if (distFLtoT < distFRtoT)
    {
        FLisPiv = true;
        distPivToT = sqrtf(distFLtoT);
    }
    else
    {
        FLisPiv = false;
        distPivToT = sqrtf(distFRtoT);
    }

    //Find the distance from the pivot to the midpoint
    float distPivToMid = sqrt(powf((o.FR.x - o.FL.x) / 2, 2) + powf((o.FR.y - o.FL.y) / 2, 2));

    //Form a triangle with corners pivot, midpoint of end line and T
    float angRAngTri = acosf(distPivToMid / distPivToT);

    //Find the angle of the line from pivot to T
    Dim2D pivToT = T - (FLisPiv ? o.FL : o.FR);
    float angPivToT = GetAngleOfLine(pivToT);

    //Find the angle of the starting line
    Dim2D startLine = (FLisPiv ? o.FR - o.FL : Dim2D({ o.FL.x -o.FR.x, o.FL.y -o.FR.y }));
    float angStLine = GetAngleOfLine(startLine);

    // The angle from the pivot to T is the sum of the angle to turn, the angle in the right angled triangle and the angle of the starting line
    float finalAngle = 0.f;
    
    if (FLisPiv)
    {
        if (angPivToT > 0)
            angPivToT -= 2 * PI;

        if (angRAngTri > 0)
            angRAngTri = -angRAngTri;
        finalAngle = angPivToT - angRAngTri - angStLine;
        finalAngle = fMod(finalAngle, -2 * PI);
    }
    else
    {
        //orient all angles such that they are in the positive direction
        if (angPivToT < 0)
            angPivToT += 2 * PI;
        if (angStLine < 0)
            angStLine += 2 * PI;
        if (angRAngTri < 0)
            angRAngTri = -angRAngTri;

        finalAngle = angPivToT - angRAngTri - angStLine;
        finalAngle = fMod(finalAngle, 2 * PI);
        if (finalAngle < 0)
            finalAngle += 2 * PI;
    }

    return finalAngle;
}
bool Formation::TurnToFace(Dim2D T, FormOrientation& o)
{
    const float minTurn = 0.01f;

    float aToTurn = CalculateTurn(T, o);
    if (fabs(aToTurn) > minTurn)
    {
        Action* nAct = QueueAction();

        nAct->myType = Action::TURNING;
        nAct->magnitude = aToTurn;
        nAct->movT = aToTurn <= 0 ? o.FL : o.FR;
        nAct->timeRemaining = (size.x * fabs(aToTurn)) / walkSpeed;
        return true;
    }
    else
    {
        //if no turn found, then we need to check if the target is on the direction line
        //if it is not, then we found that the target was too close to the pivot, and we need to move differently
        Dim2D dir = { o.FL.x - o.BL.x, o.FL.y - o.BL.y };
        Dim2D start = (o.FL + o.FR) * 0.5f;

        // l1 : p = s + kd
        // T = start + k * dir
        Dim2D kDir = T - start;
        float k = kDir.x / dir.x;
        if (k > 0 && k * dir.y == kDir.y)
        {
            //on the line in positive direction
            return true;
        }
        return false;
    }
}

void Formation::BeginMarch(Dim2D T, FormOrientation& o)
{
    if (!AllUnitsInForm())
        BeginFormUp();

    //add turn if necessary

    bool turned = TurnToFace(T, o);
    
    Dim2D pivot;
    float distToTrav = 0;

    if (turned)
    {
        //find the distance between the pivot and target
        pivot = myAction ? (myAction->magnitude >= 0 ? o.FR : o.FL) : o.FL;
        float pivToTSq = powf(pivot.x - T.x, 2) + powf(pivot.y - T.y, 2);
        float midDistSq = powf((o.FR.x - o.FL.x) / 2, 2) + powf((o.FR.y - o.FL.y) / 2, 2);

        //float midToCdist = dist(start mid - C)
        distToTrav = sqrt(pivToTSq - midDistSq);


        Action* nAct = QueueAction();

        nAct->myType = nAct->MARCHING;
        nAct->magnitude = distToTrav;
        nAct->timeRemaining = distToTrav / walkSpeed;
    }
    else
    {
        SetNoCommands();
    }
}
bool Formation::ForwardMarch(float elapsed)
{
    //must all be in formation
    orient.FL.x += elapsed * orient.facingDir.x * walkSpeed;
    orient.FL.y += elapsed * orient.facingDir.y * walkSpeed;
    orient.FR.x += elapsed * orient.facingDir.x * walkSpeed;
    orient.FR.y += elapsed * orient.facingDir.y * walkSpeed;
    orient.BL.x += elapsed * orient.facingDir.x * walkSpeed;
    orient.BL.y += elapsed * orient.facingDir.y * walkSpeed;
    orient.BR.x += elapsed * orient.facingDir.x * walkSpeed;
    orient.BR.y += elapsed * orient.facingDir.y * walkSpeed;
    orient.C.x += elapsed * orient.facingDir.x * walkSpeed;
    orient.C.y += elapsed * orient.facingDir.y * walkSpeed;

    for (size_t i = 0; i < members.size(); i++)
    {
        for (size_t j = 0; j < members[0].size(); j++)
        {
            members[i][j].anchor.x += elapsed * orient.facingDir.x * walkSpeed;
            members[i][j].anchor.y += elapsed * orient.facingDir.y * walkSpeed;
        }
    }

    myAction->timeRemaining -= elapsed;
    if (myAction->timeRemaining <= 0)
    {
        return true;
    }
    return false;
}

void Formation::BeginTurn(float magnitude)
{

}
bool Formation::ContinueTurn(float elapsed)
{
    //get angle to turn
    float aToTurn = elapsed * (myAction->magnitude >= 0 ? walkSpeed : -walkSpeed);
    aToTurn /= size.x;


    //find the matrix for turning
    float tMat[2][2];
    tMat[0][0] = cosf(aToTurn);
    tMat[1][1] = tMat[0][0];
    tMat[1][0] = sinf(aToTurn);
    tMat[0][1] = -tMat[1][0];

    orient.FL.Rotate(tMat, myAction->movT);
    orient.FR.Rotate(tMat, myAction->movT);
    orient.BL.Rotate(tMat, myAction->movT);
    orient.BR.Rotate(tMat, myAction->movT);
    orient.C.Rotate(tMat, myAction->movT);

    for (size_t i = 0; i < members.size(); i++)
    {
        for (size_t j = 0; j < members[0].size();j++)
        {
            members[i][j].anchor.Rotate(tMat, myAction->movT);
        }
    }


    //if (myAction->magnitude <= 0)
    //{
    //    //FL will stay the same, everything else rotates w no pivot
    //    Dim2D newPnt = { 0,0 };
    //    newPnt.x = FR.x * tMat[0][0] + FR.y * tMat[0][1];
    //    newPnt.y = FR.x * tMat[1][0] + FR.y * tMat[1][1];
    //    FR = newPnt;

    //    newPnt.x = BL.x * tMat[0][0] + BL.y * tMat[0][1];
    //    newPnt.y = BL.x * tMat[1][0] + BL.y * tMat[1][1];
    //    BL = newPnt;

    //    newPnt.x = BR.x * tMat[0][0] + BR.y * tMat[0][1];
    //    newPnt.y = BR.x * tMat[1][0] + BR.y * tMat[1][1];
    //    BR = newPnt;

    //    newPnt.x = C.x * tMat[0][0] + C.y * tMat[0][1];
    //    newPnt.y = C.x * tMat[1][0] + C.y * tMat[1][1];
    //    C = newPnt;

    //    Dim2D FLtoFR = FR.unit();
    //    Dim2D FLtoBL = BL.unit();

    //    for (unsigned int i = 0; i < members.size(); i++)
    //    {
    //        for (unsigned int j = 0; j < members[0].size(); j++)
    //        {
    //            members[i][j].anchor = FLtoFR * ((2*i + 1) * cellSize.x / 2) + FLtoBL * ((2*j + 1) * cellSize.y / 2);
    //        }
    //    }
    //}
    //else
    //{
    //    Dim2D FLoriginal = FL;
    //    Dim2D FRreal = FR.GetRelative(FL);
    //    Dim2D BLreal = BL.GetRelative(FL);
    //    Dim2D BRreal = BR.GetRelative(FL);

    //    Dim2D newPnt = { 0,0 };
    //    FL = FL - FRreal;
    //    newPnt.x = FL.x * tMat[0][0] + FL.y * tMat[0][1];
    //    newPnt.y = FL.x* tMat[1][0] + FL.y * tMat[1][1];
    //    FL = newPnt + FRreal;

    //    BLreal = BLreal - FRreal;
    //    newPnt.x = BLreal.x * tMat[0][0] + BLreal.y *  tMat[0][1];
    //    newPnt.y = BLreal.x * tMat[1][0] + BLreal.y * tMat[1][1];
    //    BLreal = newPnt + FRreal;
    //    BL = BLreal - FLoriginal;

    //    BRreal = BRreal - FRreal;
    //    newPnt.x = BRreal.x * tMat[0][0] + BRreal.y *  tMat[0][1];
    //    newPnt.y = BRreal.x * tMat[1][0] + BRreal.y * tMat[1][1];
    //    BRreal = newPnt + FRreal;
    //    BR = BRreal - FLoriginal;

    //    //translate all corners such that FL is (0,0)
    //    Dim2D deltaFL = FL - FLoriginal;
    //    
    //    BL = BL - deltaFL;
    //    BR = BR - deltaFL;
    //    FR = FR - deltaFL;

    //    //Get all the points in real coordinates
    //    // FL starts at (0,0)
    //    // Therefore all changes to FL must be unapplied on all other points

    //    // For each point
    //    //  Subtract FR, as this puts it around origin
    //    //  Apply rotation matrix
    //    //  Add FR, such that the rotation occured around FR

    //    // When rotating


    //    //fill in the centre
    //    Dim2D FLtoFR = FR.unit();
    //    Dim2D FLtoBL = BL.unit();

    //    C = FLtoFR * size.x + FLtoBL * size.y;

    //    for (unsigned int i = 0; i < members.size(); i++)
    //    {
    //        for (unsigned int j = 0; j < members[0].size(); j++)
    //        {
    //            members[i][j].anchor = FLtoFR * ((2 * i + 1) * cellSize.x / 2) + FLtoBL * ((2 * j + 1) * cellSize.y / 2);
    //        }
    //    }
    //}

    //rotate all corners and the FormSlots

    //recalculate the facing dir
    orient.facingDir = { orient.FR.y - orient.FL.y, -(orient.FR.x - orient.FL.x) };
    orient.facingDir = orient.facingDir.unit();

    if (SHOULD_ROTATE)
    {
        for (size_t i = 0; i < members.size(); i++)
        {
            for (size_t j = 0; j < members[0].size(); j++)
            {
                FormSlot& fs = members[i][j];
                if (fs.unit)
                {
                    fs.unit->SetDirection(orient.facingDir);
                }
            }
        }
    }

    myAction->timeRemaining -= elapsed;
    return (myAction->timeRemaining <= 0);
}

void Formation::BeginFormUp()
{
    ClearActions();
    Action* nAct = QueueAction();
    nAct->myType = myAction->MANOUEVER;

    if (myMan)
        myMan->Empty();
    else
        myMan = new Manouever;

    myMan->Begin(myMan->FORM_UP, this);
}
bool Formation::UpdateManouever(float elapsed)
{
    bool fin = myMan->Update(elapsed);
    if (fin)
        delete myMan;
    return fin;
}

bool Formation::Hold(float elapsed)
{
    myAction->timeRemaining -= elapsed;
    return (myAction->timeRemaining <= 0);
}

bool Formation::AllUnitsInForm()
{
    assert(UnitsScattered >= 0);
    return (!UnitsScattered);
}

void Formation::ClearActions()
{
    while (myAction)

    {
        Action* act;
        act = myAction;
        myAction = myAction->nextAct;
        delete act;
    }
}
void Formation::SetNoCommands()
{
    ClearActions();
    if (AllUnitsInForm())
    {
        myAction = new Action;
        myAction->myType = Action::HOLDING;
        myAction->magnitude = 0.01f;
    }
    else
    {
        BeginFormUp();
    }
}
void Formation::NextAction()
{
    if (myAction->nextAct)
    {
        Action* act;
        act = myAction;
        myAction = myAction->nextAct;
        delete act;
    }
    else
        SetNoCommands();

}


Action* Formation::QueueAction()
{
    Action* newAct = new Action;    //create the action object
    if (!myAction)
        myAction = newAct;
    else
    {
        Action* curr = myAction;    //start with beginning of queue
        while (curr->nextAct)       //if there is a following action, move to check that
            curr = curr->nextAct;
        curr->nextAct = newAct;
    }
    return newAct;
}

void Formation::Draw(Camera& cam)
{
    //begin with drawing lines between the corners
    sf::VertexArray Line;

    sf::Vector2f newPos = sf::Vector2f(orient.FL.x, orient.FL.y);
    Line.append(newPos);
    newPos = sf::Vector2f(orient.FR.x, orient.FR.y);
    Line.append(newPos);
    Line.append(newPos);
    newPos = sf::Vector2f(orient.BR.x, orient.BR.y);
    Line.append(newPos);
    Line.append(newPos);
    newPos = sf::Vector2f(orient.BL.x,orient.BL.y);
    Line.append(newPos);
    Line.append(newPos);
    newPos = sf::Vector2f(orient.FL.x, orient.FL.y);
    Line.append(newPos);

    Line.setPrimitiveType(sf::PrimitiveType::Lines);
    /*window.draw(Line);*/

    assert(members.size() > 0);
    for (size_t i = 0; i < members.size(); i++)
    {
        for (size_t j = 0; j < members[0].size(); j++)
        {
            members[i][j].unit->Draw(cam);
        }
    }
}