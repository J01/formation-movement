#include "TextureDictionary.h"


const sf::Texture& TextureDictionary::Get(TCode tC)
{
    return ((*entries)[tC]).tex;
}

TCode TextureDictionary::AddTexture(const std::string& file)
{
    if (!entries)
        entries = new std::vector<TexDictEntry>;

    bool duplicateFound = false;
    size_t  i = 0;
    size_t endI = entries->size();
    int overwritePos = -1;

    //find whether or not the file should be added
    while (!duplicateFound && i < endI)
    {
        duplicateFound = ((*entries)[i].file == file);
        if (overwritePos == -1)
            overwritePos = (*entries)[i].file == "" ? i : overwritePos;
        i++;
    }

    TCode toReturn;
    if (!duplicateFound)
    {
        sf::Texture newTex;
        newTex.loadFromFile(file);
        if (overwritePos != -1)
        {
            (*entries)[overwritePos].tex = newTex;
            (*entries)[overwritePos].file = file;
            toReturn = overwritePos;
        }
        else
        { 
            entries->push_back(TexDictEntry{ newTex, file });
            toReturn = entries->size() - 1;
        }
    }
    else
    {
        toReturn = i - 1;
    }
    return toReturn;

}
void TextureDictionary::DeleteTexture(const std::string& file)
{
    bool shouldDelete = false;
    size_t i = entries ? entries->size() : 0;
    while (!shouldDelete && i > 0)
    {
        shouldDelete = ((*entries)[i - 1].file == file);
        i--;
    }

    if (shouldDelete)
    {
        (*entries)[i].file = "";
        (*entries)[i].tex = sf::Texture();
    }
}
void TextureDictionary::Clear()
{
    if (entries)
        delete entries;
}
void TextureDictionary::ReserveSpaces(unsigned int num)
{
    if (!entries)
        entries = new std::vector<TexDictEntry>;
    entries->resize(entries->size() + num);
}