#pragma once

#include <cstdlib>
#include <math.h>
#include <assert.h>
#include <string>

#include "Formation.h"
#include "Weapons.h"
#include "Utils.h"
#include "Camera.h"




struct Weapon;
struct Armour
{
    std::string name = "";
    MinMax resistance = { 0,0 };
    int evadeBonus = 0;
    int hitBonus = 0;
    float weight = 0.f;
};
struct Shield
{
    std::string name = "";
    int hitBonus = 0;
    int evadeBonus = 0;
    float weight = 0.f;

    sf::Texture* tex;
};



struct Formation;

struct Unit
{
    //COMBAT
    Unit* currTarget = nullptr;
    Timer attTimer;
    float atkChargeTime = 0.f;
    bool inCombat = false;

    //FORMATION
    Formation* myForm = nullptr;
    IntCoord formPos;


    bool inForm = true;
    Dim2D pos;
    Dim2D GetPos();
    void EnterForm();
    void LeaveForm();

    //RENDERING
    sf::Sprite spr;

    //PHYSICAL TRAITS
    int health = 0; 
    float chargeSpeed = 0.f;
    
    float currFatigue = 0.f;
    bool fatigued = false;

    bool Update(float elapsed);

    bool TakeDamage(int damAmount);
    void AddFatigue(float fatigue);
    void DecayFatigue(float decayTime);

    void SetDirection(Dim2D dir);
    void SetAngle(float ang);
    void Draw(Camera& cam);
};



