#include "Game.h"

void Game::InitialiseTeam(Team& team, TeamInit tInit)
{
    team.forms.reserve(tInit.numForms);

    //for each formation in the team
    for (int i = 0; i < tInit.numForms; i++)
    {
        FormationCreator fc;
        fc.fm = &team.forms[i];

    }
}
void Game::InitialiseTeams(TeamInit pTeamInit, TeamInit eTeamInit)
{

}
void Game::Initialise(TeamInit pTeamInit, TeamInit eTeamInit)
{
    // Initialise teams using team initialisers
    InitialiseTeams(pTeamInit, eTeamInit);

}

void Game::CreateObjectLinks()
{
    playerTeam.myTeam.game = this;
    enemyTeam.game = this;
}

void Game::Update(float elapsed)
{
    playerTeam.Update(elapsed);
    enemyTeam.Update(elapsed);
    projectileSys.Update(elapsed);
}
void Game::Draw()
{
    playerTeam.Draw(*camera);
    enemyTeam.Draw(*camera);
    projectileSys.Draw(*camera);
}
