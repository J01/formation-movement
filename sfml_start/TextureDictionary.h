#pragma once

#include "SFML/Graphics.hpp"
#include <vector>

//  The texure dictionary contains every texture needed, loaded before the game begins
//  and stored centrally. It is more than likely that multiple objects will use the same
//  texture, so this reduces memory usage

typedef unsigned int TCode;


//There will always be a texture dictionary
// Holds one of every texture being used

// The textures and filenames are stored separately on the heap so they can be dynamically resized,
// without having to reassign pointers to the dictionary, which is stored on the stack

// The dictionary uses
struct TexDictEntry     //to store each entry of one texture and one filename
{
    sf::Texture tex;        
    std::string file;
};

static struct TextureDictionary
{
    std::vector<TexDictEntry>* entries = nullptr;

    //Function to add a texture to the dictionary
    // IN   :   
    // OUT  :   texture code, which can be used to query the dictionary on the Get(TCode) function
    //IN/OUT:   file path of the texture to be loaded
    TCode AddTexture(const std::string&);

    //Function to set the size of the dictionary
    // IN   :   numTex, the desired size of the dictionary (number of textures)
    // OUT  :   
    //IN/OUT:   
    void ReserveSpaces(const unsigned int numTex);

    //Function to delete a texture from the dictionary
    // IN   :   
    // OUT  :   
    //IN/OUT:   file path of the texture to be deleted
    void DeleteTexture(const std::string&);

    //Function to get a texture reference from the dictionary
    // IN   :   TCode, the position of the texture in the dictionary 
    // OUT  :   Texture&, texture reference as requested
    //IN/OUT:   
    const sf::Texture& Get(TCode tC);

    //Function to clear the away
    void Clear();
};
